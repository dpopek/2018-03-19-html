function showForm(container) {
    var container = "#" + container;

    var form = $("<form action='form_sent.html' method='get'></form>");
    $(container).append(form);

    var fieldset = $("<fieldset></fieldset>").append($("<legend>Formularz kontaktowy</legend>"));
    $(form).append(fieldset);

    $(fieldset).append("<p>Email: <input name='email' type='email' required='true'/></p>");
    $(fieldset).append("<p>Numer telefonu: <input name='phone_number' type='text'/></p>");
    $(fieldset).append("<p>Imię i nazwisko: <input name='name' type='text'/></p>");

    $(fieldset).append("<p>Wybierz kurs:</p>");
    $(fieldset).append("<input type='checkbox' name='course' value='Junior Java Developer'/>Junior Java Developer" +
        "<br/></p>");
    $(fieldset).append("<input type='checkbox' name='course' value='Junior Python Developer'/>Junior Python Developer" +
        "<br/></p>");
    $(fieldset).append("<input type='checkbox' name='course' value='Spring i Hibernate'/>Spring i Hibernate" +
        "<br/></p>");
    $(fieldset).append("<input type='checkbox' name='course' value='Junior Front-end Developer'/>Junior Front-end Developer" +
        "<br/></p>");

    $(fieldset).append("<p>Wybierz formę płatności:</p>");
    $(fieldset).append("<input type='radio' name='payment' value='Przelew' />Przelew");
    $(fieldset).append("<input type='radio' name='payment' value='Kredyt' />Kredyt<br/>");

    $(fieldset).append("<input type='submit' value='Wyślij formularz'/>")
}