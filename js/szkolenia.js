var coursesList = [
    {
        "nazwa": "Junior Java Developer",
        "termin": "20.06.2018",
        "liczba_miejsc": "10",
        "prowadzacy": "Adam B.",
        "tematyka": [
            {
                "nazwa": "Temat 1",
                "opis": "Course 1. Lorem ipsum dolor sit amet, ei sea nobis exerci denique, per ne bonorum mediocrem voluptaria. "
            },
            {
                "nazwa": "Temat 2",
                "opis": "Course 2. Lolor definiebas has id. An nam quando soleat, cum ei sonet soleat facilis. Amet atomorum senserit " +
                "an vis aeque menandri dissentias sit id, ne his clita volutpat."
            },
            {
                "nazwa": "Temat 3",
                "opis": "Course 3. Vim in facilisi abhorreant expetendis, cum et ornatus phaedrum. Quidam consequat reprimique pri ei, " +
                "has cu ferri"
            }
        ]
    },
    {
        "nazwa": "Junior Python Developer",
        "termin": "20.07.2018",
        "liczba_miejsc": "10",
        "prowadzacy": "Barbara C.",
        "tematyka": [
            {
                "nazwa": "Temat 1",
                "opis": "Course 2. Lorem ipsum dolor sit amet, ei sea nobis exerci denique, per ne bonorum mediocrem voluptaria. "
            },
            {
                "nazwa": "Temat 2",
                "opis": "Course 2. Lolor definiebas has id. An nam quando soleat, cum ei sonet soleat facilis. Amet atomorum senserit " +
                "an vis aeque menandri dissentias sit id, ne his clita volutpat."
            },
            {
                "nazwa": "Temat 3",
                "opis": "Course 2. Vim in facilisi abhorreant expetendis, cum et ornatus phaedrum. Quidam consequat reprimique pri ei, " +
                "has cu ferri"
            }
        ]
    },
    {
        "nazwa": "Spring i Hibernate",
        "termin": "1.07.2018",
        "liczba_miejsc": "12",
        "prowadzacy": "Cezary D.",
        "tematyka": [
            {
                "nazwa": "Temat 1",
                "opis": "Lorem ipsum dolor sit amet, ei sea nobis exerci denique, per ne bonorum mediocrem voluptaria. "
            },
            {
                "nazwa": "Temat 2",
                "opis": "Lolor definiebas has id. An nam quando soleat, cum ei sonet soleat facilis. Amet atomorum senserit " +
                "an vis aeque menandri dissentias sit id, ne his clita volutpat."
            },
            {
                "nazwa": "Temat 3",
                "opis": "Vim in facilisi abhorreant expetendis, cum et ornatus phaedrum. Quidam consequat reprimique pri ei, " +
                "has cu ferri"
            }
        ]
    },
    {
        "nazwa": "Junior Front-end Developer",
        "termin": "12.08.2018",
        "liczba_miejsc": "20",
        "prowadzacy": "Marcin G.",
        "tematyka": [
            {
                "nazwa": "Temat 1",
                "opis": "Lorem ipsum dolor sit amet, ei sea nobis exerci denique, per ne bonorum mediocrem voluptaria. "
            },
            {
                "nazwa": "Temat 2",
                "opis": "Lolor definiebas has id. An nam quando soleat, cum ei sonet soleat facilis. Amet atomorum senserit " +
                "an vis aeque menandri dissentias sit id, ne his clita volutpat."
            },
            {
                "nazwa": "Temat 3",
                "opis": "Vim in facilisi abhorreant expetendis, cum et ornatus phaedrum. Quidam consequat reprimique pri ei, " +
                "has cu ferri"
            }
        ]
    }
]

function showCourses(container) {

    var container = document.getElementById(container);

    var aside = document.createElement("aside");
    container.appendChild(aside);

    var asideList = document.createElement("ul");
    asideList.innerHTML = "Wybrane kursy";
    aside.appendChild(asideList);

    container.appendChild(createCoursesTable(asideList));


}

var mainTableHeaders = ["Nazwa szkolenia", "Termin", "Liczb miejsc","Prowadzący","Tematyka", "Dodaj"];
var subjectTableHeaders = ["Nazwa", "Opis"];

function createCoursesTable(asideList) {
    var coursesTable = document.createElement("table");
    coursesTable.appendChild(createHeaderRow(mainTableHeaders));
    for(item in coursesList){
        coursesTable.appendChild(createDataRow(item, asideList))
    }
    return coursesTable;
}

function createHeaderRow(headersList) {
    var tr = document.createElement("tr");
    var headers = headersList;
    for (item in headers){
        var th = document.createElement("th");
        th.innerHTML = headers[item];
        tr.appendChild(th);
    }
    return tr;
}

function createDataRow(courseIndex, asideList) {
    var tr = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    var td3 = document.createElement("td");
    var td4 = document.createElement("td");
    var td5 = document.createElement("td");
    var td6 = document.createElement("td");

    td1.innerHTML = coursesList[courseIndex].nazwa;
    td2.innerHTML = coursesList[courseIndex].termin;
    td3.innerHTML = coursesList[courseIndex].liczba_miejsc;
    td4.innerHTML = coursesList[courseIndex].prowadzacy;
    td5.appendChild(createSubjectTable(courseIndex));
    td6.appendChild(createAddButton(courseIndex, asideList));

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    tr.appendChild(td5);
    tr.appendChild(td6);

    return tr;
}

function createSubjectTable(courseIndex) {
    var subjectTable = document.createElement("table");
    subjectTable.appendChild(createHeaderRow(subjectTableHeaders));

    for (item in coursesList[courseIndex].tematyka){
        var tr = document.createElement("tr");

        var td1 = document.createElement("td");
        td1.innerHTML = coursesList[courseIndex].tematyka[item].nazwa;
        var td2 = document.createElement("td");
        td2.innerHTML = coursesList[courseIndex].tematyka[item].opis;

        tr.appendChild(td1);
        tr.appendChild(td2);
        subjectTable.appendChild(tr);
    }
    return subjectTable;
}

function createAddButton(courseIndex, asideList) {
    var td = document.createElement("td");
    var button = document.createElement("button");
    button.type = "submit";
    button.innerHTML = "Dodaj";
    td.appendChild(button);
    button.onclick = function () {addToAsideList(courseIndex, asideList)};
    return td;
}

function addToAsideList(courseIndex, asideList) {
    var list = asideList;
    var li = document.createElement("li");
    li.innerHTML = coursesList[courseIndex].nazwa;
    list.appendChild(li);
}

