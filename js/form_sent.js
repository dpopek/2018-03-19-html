function showFormSent(container) {
    var container = "#" + container;

    $(container).append("<header><h2>Dziękujemy za kontakt</h2></header>");
    $(container).append("<p>Lorem ipsum dolor sit amet, eos porro consulatu ad, qui ne nisl nibh, et idque dolorum duo. Ut est stet\n" +
        "            unum utamur, ad oblique diceret vel. An interesset temporibus cum. Quo te sapientem appellantur. Ut est\n" +
        "            voluptaria adipiscing, dicta efficiendi at sed. Eam nisl vocent numquam no, id nostro democritum\n" +
        "            definitionem sit.</p>");
    $(container).append("<header><h3>Przesłane informacje</h3></header>");

    var table = $("<table></table>");
    $(container).append(table);


    var url_string = window.location.href;
    var url = new URL(url_string);

    var email = url.searchParams.get("email");
    var phoneNumber = url.searchParams.get("phone_number");
    var name = url.searchParams.get("name");
    var courses = url.searchParams.getAll("course");
    var payment = url.searchParams.get("payment");

    $(table).append(createTableRow("Email",email));
    $(table).append(createTableRow("Numer telefonu",phoneNumber));
    $(table).append(createTableRow("Imię i nazwisko",name));
    $(table).append(createTableRow("Wybrane kursy",courses));
    $(table).append(createTableRow("Forma płatności",payment));
}

function createTableRow(value1, value2) {
    return $("<tr></tr>").append($("<td></td>").text(value1)).append($("<td></td>").text(value2));

}
